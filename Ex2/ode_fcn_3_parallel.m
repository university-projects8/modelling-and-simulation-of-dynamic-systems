%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This is the ODE for the Parallel Estimator
function dydt = ode_fcn_3_parallel(t,y)
   
   global a; global b; 
   
   %Input Signal   
   u=10*sin(2*t)+5*sin(7.5*t);   
   
   %States Renaming
   x=[y(1);y(2)];  x_hat=[y(3);y(4)]; a_hat=[y(5),y(6);y(7),y(8)]; b_hat=[y(9);y(10)];
   
   %Estimation of State Error (noise added)
   e=x-x_hat;
   
   %The actual system modelling
   xdot=a*x+b*u
  
   %Estimator Implementation
   a_hat_dot=x_hat*e';
   b_hat_dot=u*e';
   
   dy(1:2)=xdot;
   dy(3:4)=a_hat*x_hat+b_hat*u;
   dy(5:8)=[a_hat_dot(1),a_hat_dot(2),a_hat_dot(3),a_hat_dot(4)];
   dy(9:10)=[b_hat_dot(1),b_hat_dot(2)];

   dydt=dy';
   
end

