%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This is the ODE for the Mixed Estimator
function dydt = ode_fcn_2_mixed(t,y,theta_m,eta0, f)
   
   global a; global b; 
   
   %Input Signal
   u=5*sin(3*t);
   
   %Noise Signal Creation based on Amplitude and Frequency
   eta=eta0*sin(2*pi*f*t);
   
   %States Renaming
   x=y(1);  x_hat=y(2);  a_hat=y(3);  b_hat=y(4);  
   
   %Estimation of State Error (noise added)
   e=(x+eta)-x_hat;

   %The actual system modelling
   xdot=-a*x+b*u;

   %Estimator Implementation
   x_hat_dot=-a_hat*x_hat+b_hat*u-theta_m*e;
   a_hat_dot=-e*(x+eta);
   b_hat_dot=e*u;
    
   dy(1)=xdot;
   dy(2)=x_hat_dot;
   dy(3)=a_hat_dot;
   dy(4)=b_hat_dot;

   dydt=dy';
end

