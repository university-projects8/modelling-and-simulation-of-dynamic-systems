%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  We will simulate for some specific set of parameters the estimator:

   %  The actual values of the system's parameters:
   global a;   global b;
   a=2;  b=1;

   %  The states of the system and the estimator that will be simulated
   %  together are the following:   y(1)  =x;                              (Real System)
   %                                y(2:3)=theta_hat;
   %                                      =[a_m-a_hat;b_hat];              (Estimator)
   %                                y(4:5)=phi;
   %                                      =[x/(s+a_m);u/(s+a_m)];
   %                                      =[x*exp(-a_m*t);u*exp(-a_m*t)];  (Estimator)
   %                                y(6)  =x_hat
   %                                      =theta'*phi;                     (Estimator)
   
   
   % 3 different Parameter Settings: Comment and uncomment to change configuration
   % The values are chosen randomly in order to identify differences between
   % different configurations.
   
   a_m=1;g=10;a_hat_0=2;b_hat_0=2;max_time=10;
%  a_m=5;g=1;a_hat_0=10;b_hat_0=10;max_time=300;
%  a_m=50;g=10;a_hat_0=10;b_hat_0=10;max_time=2000;
   
   % The initial values of the states for t0=0;
   t_0=0;
   x_0=0;
   u_0=5*sin(3*t_0);
   theta_hat_0=[a_m-a_hat_0;b_hat_0];
   phi_0=exp(-a_m*t_0)*[x_0;u_0];
   x_hat_0=theta_hat_0'*phi_0;
   y_0=[x_0;theta_hat_0;phi_0;x_hat_0];

   options=odeset('RelTol',10^-10,'AbsTol',10^-11);
   [t,y]=ode45(@(t,y) ode_fcn_1(t,y,a_m,g),[0 max_time],y_0,options);  
   
   x_real=y(:,1);
   a_hat=a_m-y(:,2);
   b_hat=y(:,3);
   x_hat=y(:,6);
   
   error=x_real-x_hat;
  
   %% Plots of the simulation
   
   % States (Actual and Estimated) Plot
      figure(1)
   plot(t,x_real,'DisplayName','$x$')
   hold on
   plot(t,x_hat,'DisplayName','$\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x,\; \hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Real and Estimated State", 'Interpreter','latex')
   % Error of States Plot
      figure(2)
   plot(t,error,'DisplayName','$x-\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x-\hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Error between real and estimated parameter",'Interpreter','latex')
   % Parameter Estimation Plots
     figure(3)
   plot(t,a_hat,'DisplayName','$\hat{a}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Estimated parameter a (=2)",'Interpreter','latex')
      figure(4)
   plot(t,b_hat,'DisplayName','$\hat{b}$')
   xlabel('Time (secs)')
   ylabel('$\hat{b}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Estimated parameter b (=1)",'Interpreter','latex')
