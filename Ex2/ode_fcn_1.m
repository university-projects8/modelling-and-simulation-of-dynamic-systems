%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%   The ode function to simulate the system:
function dydt = ode_fcn_1(t,y,a_m,g)
   
   global a; global b; 
   
   %States renaming
   x=y(1);  
   theta_hat=[y(2);y(3)];  
   phi=[y(4);y(5)];  
   x_hat=y(6);
   
   % The error between x and x_hat
   e=x-x_hat;   
   
   % The input signal
   u=5*sin(3*t);
   
   % The actual system simulation:
   dx=-a*x+b*u;

   %Implement the estimator:
   theta_hat_dot=g*e*phi;
   phi_dot=-a_m*phi+[x;u];
   
   dy(1)=dx;
   dy(2)=theta_hat_dot(1); 
   dy(3)=theta_hat_dot(2);
   dy(4)=phi_dot(1);   
   dy(5)=phi_dot(2);
   dy(6)=theta_hat_dot'*phi+theta_hat'*phi_dot;
   
   dydt=dy';

end

