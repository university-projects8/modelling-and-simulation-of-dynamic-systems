%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This is a script file which simulates the estimators for some specific 
%  set of parameters and initial values.

   %  The actual values of the parameters:
   global a;   global b;
   a=2;  b=1;
   %  The states of the system and the estimator that will be simulated
   %  together are the following:   y(1)  =x;                              (Real System)
   %                                y(2)  =x_hat;
   %                                y(3)  =a_hat;
   %                                y(4)  =b_hat
     
   %  For the series-parallel structure we also need to define another
   %  parameter theta_m.
   
   theta_m=1;
   
   %  Let's find the initial values of the states for t0=0;  
   %  The actual state's initial value is given as 0.
   %  For the estimation parameters we will need to randomly choose it.
   
   t_0=0;
   x_0=0;
   u_0=5*sin(3*t_0);
   a_hat_0=0;
   b_hat_0=0;
   x_hat_0_M=0;
   x_hat_0_P=0;
   
   % Vectors with initial state values for each Estimator
   y_0_P=[x_0;x_hat_0_P;a_hat_0;b_hat_0];
   y_0_M=[x_0;x_hat_0_M;a_hat_0;b_hat_0];

   % The maximum time of simulation. Should the parameters and initial
   % values change this has to be changed as well, in order to simulate 
   % the system for enough time.
   max_time=18;
   options=odeset('RelTol',10^-10,'AbsTol',10^-11);
   [t_P,y_P]=ode45(@(t,y) ode_fcn_2_parallel(t,y,0.15,20),[0 max_time],y_0_P,options);  
   [t_M,y_M]=ode45(@(t,y) ode_fcn_2_mixed(t,y,theta_m,0.15,20),[0 max_time],y_0_M,options);  
   
   %States Renaming
   x_real_P=y_P(:,1);   
   x_hat_P=y_P(:,2);
   a_hat_P=y_P(:,3);
   b_hat_P=y_P(:,4);
   error_P=x_real_P-x_hat_P;
   
   %States Renaming
   x_real_M=y_M(:,1);   
   x_hat_M=y_M(:,2);
   a_hat_M=y_M(:,3);
   b_hat_M=y_M(:,4);
   error_M=x_real_M-x_hat_M;
   
%% Plots

%  State (actual and estimated) Parallel
          figure(1)
   plot(t_P,x_real_P,'DisplayName','$x$')
   hold on
   plot(t_P,x_hat_P,'DisplayName','$\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x,\; \hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Parallel Real and Estimated State", 'Interpreter','latex')
%  Error of States Parallel
      figure()
   plot(t_P,error_P,'DisplayName','$x-\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x-\hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Parallel error between real and estimated parameter",'Interpreter','latex')
%  Parameter Estimation Parallel
      figure(3)
   plot(t_P,a_hat_P,'DisplayName','$\hat{a}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Parallel Estimator parameter a (=2)",'Interpreter','latex')
      figure(4)
   plot(t_P,b_hat_P,'DisplayName','$\hat{b}$')
   xlabel('Time (secs)')
   ylabel('$\hat{b}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Parallel Estimator parameter b (=1)",'Interpreter','latex')
 
 %  State (actual and estimated) Mixed
        figure(5)
   plot(t_M,x_real_M,'DisplayName','$x$')
   hold on
   plot(t_M,x_hat_M,'DisplayName','$\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x,\; \hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Mixed Real and Estimated State", 'Interpreter','latex')
%  Error of States Mixed
      figure(6)
   plot(t_M,error_M,'DisplayName','$x-\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x-\hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Mixed error between real and estimated parameter",'Interpreter','latex')
%  Parameter Estimation Mixed
      figure(7)
   plot(t_M,a_hat_M,'DisplayName','$\hat{a}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Mixed Estimator, parameter a (=2)",'Interpreter','latex')
      figure(8)
   plot(t_M,b_hat_M,'DisplayName','$\hat{b}$')
   xlabel('Time (secs)')
   ylabel('$\hat{b}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title("Mixed Estimator, parameter b (=1)",'Interpreter','latex')
      
