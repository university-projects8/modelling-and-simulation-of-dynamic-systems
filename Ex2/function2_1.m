%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
% This function simulates the estimator calling the ode_fcn_1 function file to simulate the
% system. Its inputs are the desing parameters and its output is the time needed
% to achieve different levels of accuracy in state (error).

function [t4,t6,t8,t10] = function2_1(a_m,g,a_hat_0,b_hat_0, max_time)
%  The real values of the parameters:
global a;   global b;
a=2;  b=1;
   %  The states of the system and the estimator that will be simulated
   %  together are the following:   y(1)  =x;                              (Real System)
   %                                y(2:3)=theta_hat;
   %                                      =[a_m-a_hat;b_hat];              (Estimator)
   %                                y(4:5)=phi;
   %                                      =[x/(s+a_m);u/(s+a_m)];
   %                                      =[x*exp(-a_m*t);u*exp(-a_m*t)];  (Estimator)
   %                                y(6)  =x_hat
   %                                      =theta'*phi;                     (Estimator)
   %  The initial values of the states for t0=0;  
   t_0=0;
   x_0=0;
   u_0=5*sin(3*t_0);
   theta_hat_0=[a_m-a_hat_0;b_hat_0];
   phi_0=exp(-a_m*t_0)*[x_0;u_0];
   x_hat_0=theta_hat_0'*phi_0;
   y_0=[x_0;theta_hat_0;phi_0;x_hat_0];

   options=odeset('RelTol',10^-10,'AbsTol',10^-11);
   [t,y]=ode45(@(t,y) ode_fcn_1(t,y,a_m,g),[0 max_time],y_0,options);  
   
   x_real=y(:,1);
   a_hat=a_m-y(:,2);
   b_hat=y(:,3);
   x_hat=y(:,6);
   
   % Error Values to be reached based on absolute value.
   % In this for loop we iterate from the end of the loop and back.
   % As soon as we find a value greater than 1e-"x" we conclude that 
   % from this moment and after that, we have reached the error at a
   % 1e-"x" level.
   error=abs(x_real-x_hat);
   t4=0;t6=0;t8=0;t10=0;
for i=length(error):-1:1
   if (error(i)>=1e-4)
      t4=round(t(i)*100)/100;
      break;
   elseif (error(i)>=1e-6)
      t6=round(t(i)*1000)/1000;
   elseif (error(i)>=1e-8)
      t8=round(t(i)*1000)/1000;      
   elseif (error(i)>=1e-10)
      t10=round(t(i)*1000)/1000;
   end
end
