%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This script simulates the Estimators for different Noise Amplitude
%  values. A Set of different noise amplitudes is defined and both
%   estimators are simulated for these values.

   %  The actual values of the parameters:
   global a;   global b;
   a=2;  b=1;
   %  The states of the system and the estimator that will be simulated
   %  together are the following:   y(1)  =x;                              (Real System)
   %                                y(2)  =x_hat;
   %                                y(3)  =a_hat;
   %                                y(4)  =b_hat
     
   %  For the series-parallel structure we also need to define another
   %  parameter theta_m.
   
   theta_m=1;
   
   %  Let's find the initial values of the states for t0=0;  
   %  The actual state's initial value is given as 0.
   %  For the estimation parameters we will need to randomly choose it.
   
   t_0=0;
   x_0=0;
   u_0=5*sin(3*t_0);
   
   a_hat_0=0;
   b_hat_0=0;
   x_hat_0_M=0;
   x_hat_0_P=0;

   % Vector with initial states for Parallel Estimator   
   y_0_P=[x_0;x_hat_0_P;a_hat_0;b_hat_0];
  
   % Vector with initial states for Parallel Estimator
   y_0_M=[x_0;x_hat_0_M;a_hat_0;b_hat_0];

   max_time=100;
   options=odeset('RelTol',10^-10,'AbsTol',10^-11); 
   
   %The base Amplitude for which these values will be simulated
   eta0=0.15; 
   
    %The different Frequency Values
  f=[0;0.2;2;200;2000;20000;100000]; 
      
	for j=6:6
         [t_P,y_P]=ode45(@(t,y) ode_fcn_2_parallel(t,y,eta0,f(j)),[0 max_time],y_0_P,options);  
         [t_M,y_M]=ode45(@(t,y) ode_fcn_2_mixed(t,y,theta_m,eta0,f(j)),[0 max_time],y_0_M,options);  

         %States Renaming Parallel
         x_real_P=y_P(:,1);   
         x_hat_P=y_P(:,2);
         a_hat_P=y_P(:,3);
         b_hat_P=y_P(:,4);
         error_P=x_real_P-x_hat_P;

         %States Renaming Mixed
         x_real_M=y_M(:,1);   
         x_hat_M=y_M(:,2);
         a_hat_M=y_M(:,3);
         b_hat_M=y_M(:,4);
         error_M=x_real_M-x_hat_M;
         
      %The errors of the estimators are plotted once together in the same total plot
      %and once in 2 separate subplots in the same figure, for clarity purposes.
      
      figure()
      plot(t_P,error_P,'DisplayName','Parallel')
      hold on
      plot(t_M,error_M,'DisplayName','Mixed')
      xlabel('Time (secs)')
      ylabel('$x - \hat{x}$','Interpreter', 'latex')
      legend('Location','northeast','Interpreter', 'latex')
      title("State Error of Both Estimators", 'Interpreter','latex')
     
      figure()
      subplot(2,1,1)
      plot(t_P,error_P,'DisplayName','Parallel')
      title("Parallel Estimator", 'Interpreter','latex')
      xlabel('Time (secs)')
      ylabel('$x - \hat{x}$','Interpreter', 'latex')
      subplot(2,1,2)
      plot(t_M,error_M,'DisplayName','Mixed')
      xlabel('Time (secs)')
      ylabel('$x - \hat{x}$','Interpreter', 'latex')
      legend('Location','northeast','Interpreter', 'latex')
      title("Mixed Estimator", 'Interpreter','latex')
      disp(j)
      
	end
