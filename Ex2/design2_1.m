%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%
% This script runs multiple simulations of the estimator with different
% parameter configurations.
%
% Create the table to be saved in an xls file
colnames={'a_m','g','a_est','b_est','t4','t6','t8','t10'};
T=array2table(zeros(64,8),'VariableNames',colnames);
% Different parameter values
a_m=[1,2,5,50];
g=[1,2,5,10];
ab_hat_0=[1,2,10,100];
max_time=10000;
tic
%Different simulations for each parameters' combination
counter=1;
for i=1:4
   for j=1:4
      for k=1:4
         [t4,t6,t8,t10]=function2_1(a_m(i),g(j),ab_hat_0(k),ab_hat_0(k),max_time);
            T(counter,:)=array2table([a_m(i),g(j),ab_hat_0(k),ab_hat_0(k),t4,t6,t8,t10]); 
            counter=counter+1;
      end
   end
end
writetable(T,"2.1.Design.xls");

%% 1st Plot: Changing initial estimations

y11=T.t4(1:4:end);
y22=T.t4(2:4:end);
y33=T.t4(3:4:end);
y44=T.t4(4:4:end);
sim_axis=1:16;
   figure()
   plot(sim_axis,y11,'DisplayName','$\hat{a}=\hat{b}=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$\hat{a}=\hat{b}=2$')
   plot(sim_axis,y33,'DisplayName','$\hat{a}=\hat{b}=10$')
   plot(sim_axis,y44,'DisplayName','$\hat{a}=\hat{b}=100$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different Initial Estimations")
   
%% 2nd Plot: Changing initial estimations for small a_m values
y11=T.t4(1:4:48);
y22=T.t4(2:4:48);
y33=T.t4(3:4:48);
y44=T.t4(3:4:48);
sim_axis=1:12;
   figure()
   plot(sim_axis,y11,'DisplayName','$\hat{a}=\hat{b}=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$\hat{a}=\hat{b}=2$')
   plot(sim_axis,y33,'DisplayName','$\hat{a}=\hat{b}=10$')
   plot(sim_axis,y44,'DisplayName','$\hat{a}=\hat{b}=100$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different Initial Estimations")
%% 3rd Plot: Changing a_m parameter

y11=T.t4(1:16);
y22=T.t4(17:32);
y33=T.t4(33:48);
y44=T.t4(49:64);
%Ensure that those not achieving the desired levels of error
%are shown as max value (not zero). 
for i=1:length(y44)
   if y44(i)==0
      y44(i)=10000;
   end
end
sim_axis=1:16;
   figure()
   plot(sim_axis,y11,'DisplayName','$a_m=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$a_m=2$')
   plot(sim_axis,y33,'DisplayName','$a_m=5$')
   plot(sim_axis,y44,'DisplayName','$a_m=50$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different $a_m$ parameter",'Interpreter','latex')

%% 4th Plot: Changing a_m parameter small values

y11=T.t4(1:16);
y22=T.t4(17:32);
y33=T.t4(33:48);
   figure()
   plot(sim_axis,y11,'DisplayName','$a_m=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$a_m=2$')
   plot(sim_axis,y33,'DisplayName','$a_m=5$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different $a_m$ parameter",'Interpreter','latex')

%% 5th Plot: Changing gamma value

y11=[T.t4(1:4);T.t4(17:20);T.t4(33:36);T.t4(49:52)];
y22=[T.t4(5:8);T.t4(21:24);T.t4(37:40);T.t4(53:56)];
y33=[T.t4(9:12);T.t4(25:28);T.t4(41:44);T.t4(57:60)];
y44=[T.t4(13:16);T.t4(29:32);T.t4(45:48);T.t4(61:64)];

   figure()
   plot(sim_axis,y11,'DisplayName','$\gamma=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$\gamma=2$')
   plot(sim_axis,y33,'DisplayName','$\gamma=5$')
   plot(sim_axis,y44,'DisplayName','$\gamma=10$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different $\gamma$ parameter",'Interpreter','latex')

%% 6th Plot: Changing gamma value for small a-m values
   
y11=[T.t4(1:4);T.t4(17:20);T.t4(33:36)];
y22=[T.t4(5:8);T.t4(21:24);T.t4(37:40)];
y33=[T.t4(9:12);T.t4(25:28);T.t4(41:44)];
y44=[T.t4(13:16);T.t4(29:32);T.t4(45:48)];
sim_axis=1:12;
   figure()
   plot(sim_axis,y11,'DisplayName','$\gamma=1$')
   hold on
   plot(sim_axis,y22,'DisplayName','$\gamma=2$')
   plot(sim_axis,y33,'DisplayName','$\gamma=5$')
   plot(sim_axis,y44,'DisplayName','$\gamma=10$')
   xlabel('Simulations')
   ylabel('$t4$','Interpreter', 'latex')
   legend('Location','northwest','Interpreter', 'latex')
   title("Different $\gamma$ parameter",'Interpreter','latex')

