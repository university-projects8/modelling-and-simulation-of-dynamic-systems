%% Modelling and Simulation of Dynamic Systems Assignment 2 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This is a script file which simulates the estimators for some specific 
%  set of parameters and initial values.
%
%  The actual values of the parameters:
global a;   global b;
a=[-0.25,3;-5,-1];  b=[1;2.2];
   %  The states of the system and the estimator that will be simulated
   %  together are the following:   y(1:2)  =x;                              (Real System)
   %                                y(3:4)  =x_hat;
   %                                y(5:8)  =a_hat=[a11,a12,a21,a22]';
   %                                y(0:10) =b_hat=[b1,b2]'
     
   
   
   %  Let's find the initial values of the states for t0=0;  
   %  The actual state's initial value is given as 0.
   %  For the estimation parameters we will need to randomly select it.
   
   t_0=0;
   u_0=10*sin(2*t_0)+5*sin(7.5*t_0);
   x_0=[0;0];  x_hat_0=[0;0]; a_hat_0=[-10;20;-30;-50]; b_hat_0=[100;1];
   
   % Vector with initial state values for the Estimator   
   y_0_P=[x_0;x_hat_0;a_hat_0;b_hat_0];

   max_time=100;
   
   options=odeset('RelTol',10^-10,'AbsTol',10^-11);
   [t_P,y_P]=ode45(@(t,y) ode_fcn_3_parallel(t,y),[0 max_time],y_0_P,options);  
   
   %States Renaming
   x_real_P=y_P(:,1:2);   
   x_hat_P=y_P(:,3:4);
   a_hat_P=y_P(:,5:8);
   b_hat_P=y_P(:,9:10);
   error_P=x_real_P-x_hat_P;
   
%% Plots

   %State1 Actual and Estimated
   figure(1)
   plot(t_P,x_real_P(:,1),'DisplayName','$x$')
   hold on
   plot(t_P,x_hat_P(:,1),'DisplayName','$\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x,\; \hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Real and Estimated State 1', 'Interpreter','latex')
   
   %Error in State1 Estimation
   figure(2)
   plot(t_P,error_P(:,1),'DisplayName','$x-\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x-\hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel error between real and estimated State 1','Interpreter','latex')
   
   %State2 Actual and Estimated
   figure(3)
   plot(t_P,x_real_P(:,2),'DisplayName','$x$')
   hold on
   plot(t_P,x_hat_P(:,2),'DisplayName','$\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x,\; \hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Real and Estimated State 2', 'Interpreter','latex')
   
   %Error in State2 Estimation
   figure(4)
   plot(t_P,error_P(:,2),'DisplayName','$x-\hat{x}$')
   xlabel('Time (secs)')
   ylabel('$x-\hat{x}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel error between real and estimated State 2','Interpreter','latex')
   
   %Parameters Estimation
   figure(5)
   plot(t_P,y_P(:,5),'DisplayName','$\hat{a}_{11}$')
   hold on
   plot(t_P,a(1,1)*ones(length(t_P),1),'DisplayName','$a_{11}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter a_{11} (=-0.25)','Interpreter','latex')
   
   figure(6)
   plot(t_P,y_P(:,6),'DisplayName','$\hat{a}_{12}$')
   hold on
   plot(t_P,a(1,2)*ones(length(t_P),1),'DisplayName','$a_{12}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter a_{12} (=3)','Interpreter','latex')
     
   figure(7)
   plot(t_P,y_P(:,7),'DisplayName','$\hat{a}_{21}$')
   hold on
   plot(t_P,a(2,1)*ones(length(t_P),1),'DisplayName','$a_{21}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter a_{21} (=-5)','Interpreter','latex')
     
   figure(8)
   plot(t_P,y_P(:,8),'DisplayName','$\hat{a}_{22}$')
   hold on
   plot(t_P,a(2,2)*ones(length(t_P),1),'DisplayName','$a_{22}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter a_{22} (=-1)','Interpreter','latex')
   
   figure(9)
   plot(t_P,y_P(:,9),'DisplayName','$\hat{b}_{1}$')
   hold on
   plot(t_P,b(1)*ones(length(t_P),1),'DisplayName','$b_{1}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter b_{1} (=1)','Interpreter','latex')
      
   figure(10)
   plot(t_P,y_P(:,10),'DisplayName','$\hat{b}_{2}$')
   hold on
   plot(t_P,b(2)*ones(length(t_P),1),'DisplayName','$b_{2}$')
   xlabel('Time (secs)')
   ylabel('$\hat{a}$','Interpreter', 'latex')
   legend('Location','northeast','Interpreter', 'latex')
   title('Parallel Estimator parameter b_{2} (=2.2)','Interpreter','latex')
