%% Least-Squares Algorithm for Black-Box
%  simulated by out.p file. System of order n=3,m=2.
clear all; close all

tspan=0:0.001:20;
u=sin(tspan)+0.1*cos(2*tspan);
y=out(tspan,u);
len=size(tspan,2);
error=zeros(1,15);
training=[y(1:18000);y(2001:20001]
%% PARAMETERS
n=3; m=2; p=0.1;
%% Calculate filter coefficients for L(s)=1/(s+pole)^n
L=zeros(1,n);
for k=0:n
	L(k+1)=nchoosek(n,k)*p^k;
end
%% PHI (Regressor) creation
	den_y=-eye(n);
	den_u=eye(m+1);
	phi=zeros(n+m+1,len);
	for ctr=1:n+m+1
      if ctr<=n
         phi(ctr,:)=lsim(tf(den_y(ctr,:),L),y,tspan);
      else
         phi(ctr,:)=lsim(tf(den_u(ctr-n,:),L),u,tspan);
      end
   end

%% LSA Algorithm
	sum1=zeros(n+m+1,n+m+1);
	sum2=zeros(n+m+1,1);
   for i=1:len
      sum1=sum1+phi(:,i)*phi(:,i).';
      sum2=sum2+phi(:,i)*y(i);
   end
	sum1=sum1/len;      
	sum2=sum2/len;
	theta_l=(sum1)\(sum2);
   estimation_offline=theta_l+[L(2:end)';zeros(m+1,1)]; %Parameters - Coefficients
%% Plots
   system = tf([estimation_offline(4),estimation_offline(5),estimation_offline(6)],...
      [1,estimation_offline(1),estimation_offline(2),estimation_offline(3)]);
   system_response=lsim(system,u,tspan);

   figure()
   plot(tspan,y,'LineWidth',1.5);
   hold on
   plot(tspan,system_response,'r--','LineWidth',1.5)
   legend('actual','estimation')
   title('Output Measured and Estimated')
   xlabel('Time')
   ylabel('Output')   
   figure()
   plot(tspan,y-system_response,'LineWidth',1.5);
   title('Output Error')
   xlabel('Time')
   ylabel('Output')
   legend('Error')
