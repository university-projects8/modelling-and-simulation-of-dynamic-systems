clear all
close all

dt=0.0001;
t=0:dt:50;
u=sin(t).*exp(-t/10)+2.1*cos(2*t);
y=out(t,u);
y_meas=out(t,u);
len=size(t,2);
n=3; m=2;

%% OFFLINE ESTIMATOR (FOR COMPARISON)
p=1;
L=zeros(1,n);
         for k=0:n
            L(k+1)=nchoosek(n,k)*p^k;
         end
      den_y=-eye(n);
      den_u=eye(m+1);
      phi=zeros(n+m+1,len);
      for ctr=1:n+m+1
            if ctr<=n
               phi(ctr,:)=lsim(tf(den_y(ctr,:),L),y,t);
            else
               phi(ctr,:)=lsim(tf(den_u(ctr-n,:),L),u,t);
            end
      end
      sum1=zeros(n+m+1,n+m+1);
      sum2=zeros(n+m+1,1);
    
         for i=1:len
            sum1=sum1+phi(:,i)*phi(:,i).';
            sum2=sum2+phi(:,i)*y(i);
         end
         sum1=sum1/len;      
         sum2=sum2/len;
         theta_l=(sum1)\(sum2);
      
estimation_offline=theta_l+[L(2:end)';zeros(m+1,1)];

system_offline= tf([estimation_offline(n+1:end)'],[1,estimation_offline(1:n)'])
system_offline_response=lsim(system_offline,u,t);



%% ONLINE ESTIMATOR: Recursive Least Squares Algorithm
      Q_bar=1000;
      beta=200;
      R_bar=1500;

      theta_hat0=zeros(n+m+1,1); 
      Q0=Q_bar*eye(n+m+1);
      P=inv(Q0);
      theta_hat=theta_hat0;
           %% PHI Creation in DISCRETE
      phi=zeros(n+m+1,length(t));
      for j=1:length(t)    %j is each column (represents t, starting from 1)
         for k=1:n         %k is each row for y (output)
            index=j-k;
            if index>=1
               phi(k,j)=-y_meas(index);
            else
               phi(k,j)=0;
            end
         end
         for k=0:m         %k is each row for u (input)
            index=j-k;
            if index>=1
               phi(n+k+1,j)=u(index);
            else
               phi(n+k+1,j)=0;
            end
         end
      end
      %% LOOP
      for i=1:length(t)
         y_est(i)=theta_hat'*phi(:,i);
         error(i)=y_meas(i)-y_est(i);
         % P dot
         Pdot=beta*P-P*phi(:,i)*phi(:,i)'*P;
         if norm(Pdot,2)>R_bar
            Pdot=zeros(n+m+1);
         end
         %theta_hat dot
         theta_hat_dot=P*error(i)*phi(:,i);
         
         % Euler Integration
         theta_hat=theta_hat+theta_hat_dot*dt;
         P=P+Pdot*dt;
         
         %Store Parameters' Estimation History
         estimation(:,i)=theta_hat;
      end      
 

%% Build respective model
estimation_online=theta_hat;
system_online_discrete=filt([estimation_online(n+1:end)'],[1,estimation_online(1:n)'],dt)
system_online_cont=d2c(system_online_discrete)
system_online_response=lsim(system_online_cont,u,t);

%% PLOTS
figure()
plot(t,y_meas,'LineWidth',1.25); 
hold on; 
plot(t,y_est,'--','LineWidth',2)
legend('Measured','Estimation')
title('Online Estimator Output')
xlabel('Time')
ylabel('Output')
figure()
plot(t,y_meas'-y_est,'LineWidth',1.25); 
hold on
plot(t,zeros(1,length(t)))
title('Online Estimator Error')
legend('Error')
xlabel('Time')
ylabel('Error')


figure()
plot(t,y_meas,'LineWidth',2)
hold on
plot(t,system_online_response,'--','LineWidth',1.25)
legend('Actual','Online')
title('Online System Response')
xlabel('Time')
ylabel('Output')

figure()
plot(t,y_meas,'--','LineWidth',2)
hold on
plot(t,system_offline_response,'LineWidth',1.25)
plot(t,system_online_response,'LineWidth',1.25)
title('TFs Comparison')
legend('Actual','Offline','Online')
title('Systems Comparison')
xlabel('Time')
ylabel('Output')

figure()
plot(t,estimation,'LineWidth',1.25)
title('Parameters Estimated')
legend('a1','a2','a3','b0','b1','b2')
xlabel('Time')
ylabel('Parameters')













