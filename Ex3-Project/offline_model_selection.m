clear all;

pole=0.1; %Selecting the pole of the filter

n_sens=1:5;    %Values of n to be simulated

tspan=0:0.001:20;
u=sin(tspan)+0.1*cos(2*tspan);
y=out(tspan,u);

len=size(tspan,2);

error=zeros(1,15);   %Error for each simulation

fileID = fopen('sens.txt','w');
fprintf(fileID,'%1s %1s %1s \t%6s \t \t%6s\n','n','m','p','error^2','max_error');
%Let's consider that a good approximation is with 1e-6 accuracy
for p=1:size(pole,2)
   %Create a figure for each different filter-pole
figure()
ctr_fig=0;
   for j=1:size(n_sens,2)
      n=n_sens(j);   %Order of n (output)
      
      for l=1:n
         m=n_sens(l);      %Order of m (input)

      %L(s)=(s+pole)^n=Sum_{k=0}^{n} nchoosek(n,k)*pole^{k}*s^{(n-k)}
      % We will have n coefficients with the k-th being; nchoosek(n,k)*pole^k
      L=zeros(1,n);
         for k=0:n
            L(k+1)=nchoosek(n,k)*pole(p)^k;
         end
      
      % We need to create the nominators coefficients for Delta (separately for y and u)
      den_y=-eye(n);
      den_u=eye(m+1);
      
      % We will have n+m+1 phi coefficients:
      phi=zeros(n+m+1,len);
      % Each 2-D matrix (dimension F) is for a different pole.
      % Each 2-D matrix represents a filtered matrix phi.
         for ctr=1:n+m+1
            if ctr<=n
               phi(ctr,:)=lsim(tf(den_y(ctr,:),L),y,tspan);
            else
               phi(ctr,:)=lsim(tf(den_u(ctr-n,:),L),u,tspan);
            end
         end
      %% Least Squares Algorithm
      %  Now that we calculated phi and we also have y, we can apply the
      %  LSA in order to find the best value for theta.
      %  The LSA is implemented in the summation form:
      sum1=zeros(n+m+1,n+m+1);
      sum2=zeros(n+m+1,1);
    
         for i=1:len
            sum1=sum1+phi(:,i)*phi(:,i).';
            sum2=sum2+phi(:,i)*y(i);
         end
         sum1=sum1/len;      
         sum2=sum2/len;
         theta_l=(sum1)\(sum2);
         ctr_fig=ctr_fig+1;

         error(ctr_fig)=sum((y'-theta_l'*phi).^2,2);
         fprintf(fileID,'%d %d %d\t %.4g\t \t %.4g\n',n,m,pole(p),error(ctr_fig),max(abs(y'-theta_l'*phi)));
         subplot(3,5,ctr_fig)
         plot(tspan, y'-theta_l'*phi,'LineWidth',1)
         hold on
         plot(tspan,max(abs(y'-theta_l'*phi))*ones(1,length(tspan)),'r--','LineWidth',0.75)
         title(sprintf('(%d,%d)',n,m))
      end
   end
end
fclose(fileID);
