%% Modelling and Simulation of Dynamical Systems, Project
%  Jason George Velentzas, AEM: 8785, email: velentzas@ece.auth.gr
%
%  We will try to experimentally prove linearity of the system modelled from
%  out.p file. For this,  we will  check if: T{a*u1+b*u2}=a*T{u1}+b*T{u2} holds.
clear all; close all;
tspan=0:0.001:10;
sz=size(tspan,2);
% Random Values for Sinusoidal Amplitude
A=[1,1.5,2.7,12,25,33,78,100,87,0.7,2.5,32,4,pi,pi^2,pi^3,0.2, 1.25, exp(2),80];
% Random Values for Sinusoidal Frequency
f=[100,200,300,1000,10000,50000,23234,2000,789,200000,10,123,333,1000,23.4,120000,pi,100,200,10000];
u_step=ones(1,sz); %Step input
u_ramp=tspan;  %Ramp input
% Sinusodial Inputs based on random numbers
for i=1:20
   u(i,:)=A(i)*sin(2*pi*f(21-i)*tspan);
end
% Since we have implemented random inputs we will keep the values of a and b constant
a_values=[2.5,0.001,-76];
b_values=[-0.6,0.02,-99];
ctr=0;

% We will create input functions as sum of sinusoids in order to increase complexity
% Summation of up to 30 sinusoids.
% The first input will be composed from j1=1--30 sinusoids, while the second input will 
% be composed from j2=1--j1 sinusoids.
for tmp1=1:3
   a=a_values(tmp1);b=b_values(tmp1);
for j1=1:10
      input_1=zeros(1,sz); input_2=zeros(1,sz);
      for ctr1=1:j1
         input_1=input_1+u(ctr1,:);
         input_2=input_2+u(21-ctr1,:);
      end
   ctr=ctr+1;     % This counter marks tries, means the inputs are created

   input_total=a*input_1+b*input_2;
   H_1=out(tspan,input_1);
   H_2=out(tspan,input_2);
   H_total=out(tspan,input_total);
   % Now we find the maximum error value (in absolute sense)
   [max_error(ctr),index]=max(abs(H_total-a*H_1-b*H_2));
   percentage(ctr)=max_error(ctr)/abs(H_total(index))*100; 
   mean_error(ctr)=mean(abs(H_total-a*H_1-b*H_2));
end
%Now we try sinusoidal inputs with step input
for j1=1:5
      input_1=zeros(1,sz);
      for ctr1=1:j1
         input_1=input_1+u(ctr1,:);
      end
   input_2=u_step;
   
   ctr=ctr+1;     % This counter marks tries, means the inputs are created
   
   input_total=a*input_1+b*input_2;
   H_1=out(tspan,input_1);
   H_2=out(tspan,input_2);
   H_total=out(tspan,input_total);
   [max_error(ctr),index]=max(abs(H_total-a*H_1-b*H_2));
   percentage(ctr)=max_error(ctr)/abs(H_total(index))*100;
   mean_error(ctr)=mean(abs(H_total-a*H_1-b*H_2));
end
%Now we try sinusoidal inputs with ramp input
for j1=1:5
      input_1=zeros(1,sz);
      for ctr1=1:j1
         input_1=input_1+u(ctr1,:);
      end
   input_2=u_ramp;
   ctr=ctr+1;     % This counter marks tries, means the inputs are created
   
   input_total=a*input_1+b*input_2;
   H_1=out(tspan,input_1);
   H_2=out(tspan,input_2);
   H_total=out(tspan,input_total);
   [max_error(ctr),index]=max(abs(H_total-a*H_1-b*H_2));
   percentage(ctr)=max_error(ctr)/max(abs(H_total(index)),abs(a*H_1(index)+b*H_2(index)))*100;
   mean_error(ctr)=mean(abs(H_total-a*H_1-b*H_2));
end
end
figure()
plot(1:ctr,max_error,'LineWidth',1.5)
title('Maximum Error per Simulation')
legend('Max Error')
ylabel('Error')
xlabel('Simulation#')
figure()
plot(1:ctr,percentage,'LineWidth',1.5)
title('Maximum Error per Simulation (Percentage) %')
legend('Max Error(%)')
ylabel('Error(%)')
xlabel('Simulation#')
figure()
plot(1:ctr,mean_error,'LineWidth',1.5)
title('Mean Error per Simulation')
legend('Mean Error')
ylabel('Error')
xlabel('Simulation#')

%% Check if sine shape is preserved
tspan=0:0.001:20;
input1=sin(tspan)+0.1*cos(2*tspan);
input2=2*sin(tspan)+0.1*cos(2*tspan)+3*sin(10*tspan)+cos(20*tspan)+5;
output1=out(tspan,input1);
output2=out(tspan,input2);

figure()
subplot(2,2,1)
plot(tspan,input1,'b','LineWidth',1.5)
ylabel('Input')
legend('Input')
xlabel('Time')
subplot(2,2,3)
plot(tspan,output1,'r','LineWidth',1.5)
ylabel('Output')
legend('Output')
xlabel('Time')
subplot(2,2,2)
plot(tspan,input2,'b','LineWidth',1.5)
ylabel('Input')
legend('Input')
xlabel('Time')
subplot(2,2,4)
plot(tspan,output2,'r','LineWidth',1.5)
legend('Output')
ylabel('Output')
xlabel('Time')
title('Shape of Sine')

%% Check if Shift in Time property holds
t1=1000; %1second
input1=sin(tspan+t1)+0.1*cos(2*(tspan+t1));
output1=out(tspan,input1);
input2=sin(tspan)+0.1*cos(2*tspan);
output2=out(tspan,input2);

input3=2*sin(tspan+t1)+0.1*cos(2*(tspan+t1))+3*sin(10*(tspan+t1))+cos(20*(tspan+t1))+5;
output3=out(tspan,input3);
input4=2*sin(tspan)+0.1*cos(2*tspan)+3*sin(10*tspan)+cos(20*tspan)+5;
output4=out(tspan,input4);

figure()
subplot(2,2,1)
plot(tspan,input2,'b','LineWidth',1.2)
hold on
plot(tspan,input1,'r--','LineWidth',1.5)
ylabel('Input')
legend('Input')
xlabel('Time')
subplot(2,2,3)
plot(tspan,output2,'b','LineWidth',1.2)
hold on
plot(tspan,output1,'r--','LineWidth',1.5)
ylabel('Output')
legend('Output')
xlabel('Time')
subplot(2,2,2)
plot(tspan,input4,'b','LineWidth',1)
hold on
plot(tspan,input3,'r--','LineWidth',1.2)
ylabel('Input')
legend('Input')
xlabel('Time')
subplot(2,2,4)
plot(tspan,output4,'b','LineWidth',1)
hold on
plot(tspan,output3,'r--','LineWidth',1.2)
ylabel('Output')
legend('Output')
xlabel('Time')








