# Modelling and Simulation of Dynamic Systems

## Part 1

### Linear Parameterization, Parameters' Estimation and LSE

#### Part 1_A

Consider a mass-spring-damper system (b, k, m) and u(t) an external force acting on the mass (similar to [this one](https://study.com/cimages/multimages/16/1113581948220847426224.png)). Suppose y(t) is the displacement of the mass due to u(t):
- Find the linear parameterization <img src="https://latex.codecogs.com/gif.latex?y(t) = \theta^{*}\zeta " height = "16" /> of the mathematical model describing the system, where <img src="https://latex.codecogs.com/gif.latex?\zeta " /> is created by measurements of the external force and the displacement.
- Desing a Least Squares Estimator for the parameters (b,k,m) when <img src="https://latex.codecogs.com/gif.latex?\zeta " /> is considered known.
- Simulate the LSE form=15kg, b=0.2kg/sec, k=2kg/sec^2 and u(t)=5sin(2t)+10.5 N for t=0:0.1:10 sec with zero initial conditions.

#### Part 1_B
Consider an electrical circuit consisting of 2 loops with a common edge. There are two independent voltage sources v1(t)=2sin(t) V, v2(t)=1 V, one per loop. The upper loop contains a resistance R, while the lower a capacitance C. The common edge contains an inductor L. Only the voltage differences on Resistance and Capacitance, VR and VC respectively are measurable (their measurements are provided in a file v.p).
- Estimate using the Least Squares Algorithm the transfer function matrix of the circuit.
- Consider that the measurements of v.p are false (falsify randomly 3 measurements by assigning values way larger than the normal ones). Discuss the effect on the Estimator.

## Part 2

### On line parameter estimation: Gradient and Lyapunov Methods

For this exercise we consider the following dynamical system:  <img src="https://latex.codecogs.com/gif.latex?\dot{x}=-ax+bu,\;x(0)=0" />  
where x is the state vector, a= 2, b=1 and u(t) = 5sin(3t) is the input. We want to estimate the parameters a and b.

#### Part 2_A
Design an online estimator based on the Gradient Method. Create the plots of x, x_hat and a,b, a_hat, b_hat

#### Part 2_B
Design an online estimator based on the Parallel and Mixed Lyapunov Methods. Consider that the measered state contains noise n(t) = 0.15sin(40 pi t). Discuss what would happen if the amplitude and frequency of noise were different. 


#### Part 2_C

Consider the second order system: <img src="https://latex.codecogs.com/gif.latex?\dot{x}=\begin{bmatrix}a_{11}& a_{12} \\ a_{21} & a_{22}\end{bmatrix}x+\begin{bmatrix}b_{1}\\b_{2}\end{bmatrix}u,\;x(0)=\begin{bmatrix}0\\0\end{bmatrix}" /> where x is the state vector, a11 = -0.25, a12 = 3, a21 = -3, a22 = -1, b1 = 1, b2 = 2.2 and u(t) = 10sin(2t)+5sin(7.5t) is the input. We want to estimate the parameters a and b with a Parallel Lyapunov Method.


## Part 3

### Blackbox identification

Given a system with input u(t) and output y(t) for which we only know that it is causal and stable,
we want to estimate its model. Firstly, conduct an experimental simulation to indicate whether the
system is linear or not. Followingly, develop an online and an offline estimation method to identify its 
parameters and evaluate the respective models.


# Project Files

## Modelling and Simulation of Dynamic Systems Assignment 1

	This assignment consists of 2 parts. For each part there is at least one design decision to be made.  
	These decisions are implemented through a trial and error process, a rough/approximate sensitivity 
	analysis. The results are stored in .xls files which are provided in the same folder with the scripts.  
	
	Part 1_A: The design decision is about placing the poles of the filter Lambda(s).  

	--Ex1_a1.m: The file containing the final simulation after having selected filter parameters.  
	--Ex1_a1_f.m: The function file containing the simulation and taking as inputs the parameters 
	  of the filter. It is used to select which L1,L2  are the "best".  
	--Ex1_design1.m: The script that is used to implement this "sensitivity analysis". It writes 
	  in PoleSensitivityQ1.xls and PolesSensitivityQ1Round2.xls files which are stored in the 
	  current Matlab folder (path). The writing process is suppressed through commenting.  

	Part 1_B: The design decisions are about the timestep of the measurements, the max time of the 
	  measurements and also the the placement of the filter's poles.

	--Ex1_a2.m: The file containing the final simulation after having selected all parameters.  
	--Ex1_a2_f.m: The function file containing the simulation and taking as inputs, t_step, t_max 
	  and the parameters of the filter. It is used to select the "best" parameters.  
	--Ex1_design2.m: The script that is used to implement this "sensitivity analysis". It writes in
	  PoleSensitivityQ2.xls and PolesSensitivityQ2Round2.xls files which are stored in the current 
	  Matlab folder (path). The writing process is suppressed through commenting.  

	Part 1_B_spikes: The design decisions are about the order of magnitude of the added "noise". 
	More specifically, how many times higher is the order of it compared to the normal output.

	--Ex1_a2_spikes.m: The file containing the final simulation after having selected the parameter.
	  Three values of the output are modificated with "noise".
	--Ex1_a2_f_spikes.m: The function file containing the simulation that is taking as input the 
	  magnitude of the added noise.  
	--Ex1_design2_spikes.m:	The script that is used to implement this "sensitivity analysis". It 
	  writes in PoleSensitivityQ2Spikes.xls file which is stored in the current Matlab folder (path). 
	  The writing process is suppressed through commenting.

    The scripts to run are:  
    - a1.m, a2.m, a2_spikes.m, which are the final simulations having made all design decisions.
	  The output is the estimated parameters of the problem and the "theta_0" vector.
    - design1.m, design2.m, design2_spikes.m, which are the sensitivity analysis files. The output
	  is a table containing the parameter estimations. In design2_spikes there are also plots that
	  are suppressed via commenting.
															
## Modelling and Simulation of Dynamic Systems Assignment 2 

	For each of the 3 parts of this assignment there are the following types of scripts:

	-- A file that implements the necessary simulation and that is run by the user:
	  Part2_A: simulation2_1.m
	  Part2_B: simulation2_2.m
	  Part2_C: simulation2_3.m

	-- A file that implements the system's and the estimator's differential equations:
	  Part2_A: ode_fcn_1.m
	  Part2_B: ode_fcn_2_parallel.m, ode_fcn_2_mixed.m
	  Part2_C: ode_fcn_3_parallel.m	  

	There are also some extra files for some parts of the assignment:

	-- For Part2_B there are the files that implement the noise:
	  Part2_B: noiseAmple.m, noiseFreq.m
	  
	-- In Part2_A there are some degrees of freedom for the designer (parameter estimates
	  and initial values). We also keep track of the time that the estimator needs to achieve
	  different levels of accuracy (1e-4, 1e-6, 1e-8, 1e-10).
	  Part2_A: function_2_1.m, design_2_1.m, 2.1.Design.xls

## Modelling and Simulation of Dynamic Systems Assignment 3 

	The system is modelled through a given file out.p, which is called as: y = out(t,u) with a
	timestep not greater than 1ms.

	-- linearity.m:		checks whether the system can be estimated by a linear model or not.

	-- offline_est.m, online_est.m: apply an offline and online estimation method accordingly.

	-- offline_model_selection.m: 	decides about the final model to be selected.

