function theta_0 = a2_f_spikes(spike)

%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
% 
%  This script's input is the order of
%  the spike added to the noise, in order to simulate how the LSA
%  responds in different orders of noise magnitude. 


%% Model Setup
%  The RLC circuit of the problem can be modelled as follows:
%  Vc'' + (1/RC)*Vc' + (1/LC)*Vc = (1/RC)*u1' + (1/RC)*u2'+ (1/LC)*u2   (A)    
%  Vr= u1 + u2 - Vc                                                     (B)
%  The R,L,C parameters remain unknown. We only have measurements of 
%  the outputs: Vc, Vr.
%  Measurements on the defined timespan:
tspan=0:1e-5:2;
n=length(tspan);
Vc=zeros(n,1);
Vr=zeros(n,1);
%  Measurements are taken from v.p file based on each moment
for i=1:n
   Vout=v(tspan(i));
   Vc(i)=Vout(1);
   Vr(i)=Vout(2);
end
y=Vc;
%Adding random norm noise in random moments
index1=randi([1,n]);
index2=randi([1,n]);
index3=randi([1,n]);
y(index1)=y(index1)+10^spike*(y(index1)+abs(randn()));
y(index2)=y(index2)+10^spike*(y(index2)+abs(randn()));
y(index3)=y(index3)+10^spike*(y(index3)+abs(randn()));

%  For the defined timespan we can calculate the input functions:
u1=(2*sin(tspan))';
u2=ones(n,1); % constant

%% Linear Parameterization
%  In order to linearly parameterize the system, we need
%  to filter the system output, since we cannot measure
%  values of the output's derivatives. Thus, we will 
%  define the polynomial: L(s)=s^2+L1*s+L2 in order to be
%  used for the filtering. The parameters L1,L2 are selected:
L1=150; L2=75^2; 

%  The linearly parameterized system can be written as y=theta*phi, 
%  where:   theta*=[(1/RC)-L1, (1/LC)-L2, (1/RC), 0, (1/RC), (1/LC)] 
%  and      phi=[-s*y; -y; s*u1; u1; s*u2; u2]

%  We already have the output (y) and the input (u) at the specific timespan
%  so we need to filter them in order to find phi.

[phi1,~]=lsim(tf([-1,0],[1,L1,L2]),y,tspan);
[phi2,~]=lsim(tf([0,-1],[1,L1,L2]),y,tspan);
[phi3,~]=lsim(tf([1,0],[1,L1,L2]),u1,tspan);
[phi4,~]=lsim(tf([0,1],[1,L1,L2]),u1,tspan);
[phi5,~]=lsim(tf([1,0],[1,L1,L2]),u2,tspan);
[phi6,~]=lsim(tf([0,1],[1,L1,L2]),u2,tspan); 

phi=[phi1';phi2';phi3';phi4';phi5';phi6'];

%% Least Squares Algorithm
%  Now that we calculated phi and we also have y, we can apply the
%  LSA in order to find the best value for theta.
%  The LSA is implemented in the summation form:
sum1=zeros(6,6);
sum2=zeros(6,1);
for i=1:n
   sum1=sum1+phi(:,i)*phi(:,i).';
   sum2=sum2+phi(:,i)*y(i);
end
sum1=sum1/n;      
sum2=sum2/n;
theta_0=(sum1)\(sum2);
theta_0=theta_0+[L1;L2;0;0;0;0];

end
