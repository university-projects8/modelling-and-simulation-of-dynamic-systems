%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
% 
%  In this script, we call the v.p file in order to save the
%  Voltage Measurements. The input arguments are the step and max
%  value of the time of measurements, and also the filter parameters.
%  After the measurements are stored, the system is linearly para
%  meterized, so that Least-Squares Algorithm is applied afterwards
%  using only the Vc and u measurements.

%% Model Setup
%  The RLC circuit of the problem can be modelled as follows:
%  Vc'' + (1/RC)*Vc' + (1/LC)*Vc = (1/RC)*u1' + (1/RC)*u2'+ (1/LC)*u2   (A)    
%  Vr= u1 + u2 - Vc                                                     (B)
%  The R,L,C parameters remain unknown. We only have measurements of 
%  the outputs: Vc, Vr.
%  Measurements on the defined timespan:
tspan=0:1e-5:2;
n=length(tspan);
Vc=zeros(n,1);
Vr=zeros(n,1);
%  Measurements are taken from v.p file based on each moment
for i=1:n
   Vout=v(tspan(i));
   Vc(i)=Vout(1);
   Vr(i)=Vout(2);
end
y=Vc;
%  For the defined timespan we can calculate the input functions:
u1=(2*sin(tspan))';
u2=ones(n,1); % constant

%% Linear Parameterization
%  In order to linearly parameterize the system, we need
%  to filter the system output, since we cannot measure
%  values of the output's derivatives. Thus, we will 
%  define the polynomial: L(s)=s^2+L1*s+L2 in order to be
%  used for the filtering. The parameters L1,L2 are selected:
L1=2*75; L2=75^2; 

%  The linearly parameterized system can be written as y=theta*phi, 
%  where:   theta*=[(1/RC)-L1, (1/LC)-L2, (1/RC), (1/RC), (1/LC)] 
%  and      phi=[-s*y; -y; s*u1; s*u2; u2]

%  We already have the output (y) and the input (u) at the specific timespan
%  so we need to filter them in order to find phi.

[phi1,~]=lsim(tf([-1,0],[1,L1,L2]),y,tspan);
[phi2,~]=lsim(tf([0,-1],[1,L1,L2]),y,tspan);
[phi3,~]=lsim(tf([1,0],[1,L1,L2]),u1,tspan);
[phi4,~]=lsim(tf([1,0],[1,L1,L2]),u2,tspan);
[phi5,~]=lsim(tf([0,1],[1,L1,L2]),u2,tspan); 
phi=[phi1';phi2';phi3';phi4';phi5'];

%% Least Squares Algorithm
%  Now that we calculated phi and we also have y, we can apply the
%  LSA in order to find the best value for theta.
%  The LSA is implemented in the summation form:
sum1=zeros(5,5);
sum2=zeros(5,1);
for i=1:n
   sum1=sum1+phi(:,i)*phi(:,i).';
   sum2=sum2+phi(:,i)*y(i);
end
sum1=sum1/n;      
sum2=sum2/n;
theta_l=(sum1)\(sum2);
% From theta_lambda, we create theta_0.
theta_0=theta_l+[L1;L2;0;0;0];
RC1= 1/theta_0(1);RC2=1/theta_0(3);RC3=1/theta_0(4);LC1=1/theta_0(2);LC2=1/theta_0(5);
fprintf("\n Estimation of RC's:\n")
fprintf(" Estimation1:  %.12f\n", 1/theta_0(1))
fprintf(" Estimation2:  %.12f\n", 1/theta_0(3))
fprintf(" Estimation3:  %.12f\n", 1/theta_0(4))
fprintf("\n Estimation of LC's:\n")
fprintf(" Estimation1:  %.12fe-8\n", (1/theta_0(2))*10^8)
fprintf(" Estimation2:  %.12fe-8\n", (1/theta_0(5))*10^8)

clear phi1 phi2 phi3 phi4 phi5 phi sum1 sum2 i theta_l 

