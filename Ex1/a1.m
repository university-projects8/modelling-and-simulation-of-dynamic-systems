%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr
%
%  This script is the main script file. It simulates the system with
%  the given parameters and stores the measurements needed. Afterwards,
%  the system is linearly parameterized and a filter is introduced, in
%  order to be able to apply the Least-Squares Algorithm only with the
%  calculated measurements. Then, we estimate the given parameters.

%% Model Setup
%  The Spring-Damper System can be modelled as follows:
%  y'' + (b/m)*y' + (k/m)*y = (1/m)*u
%  Parameters of the System:
k=2;
b=0.2;
m=15;
%  System Modeling
%  We define the state variables: x1=y, x2=y' and 
%  the system can be modelled in matrix form as follows.
sys=ss([0,1;-k/m,-b/m],[0;1/m],[1,0],0,0.1);
% System Simulation (continuous)
x0=[0 0];   % Initial Conditions
tspan=[0 10]; % Simulation Timespan
f=@(t,state)spring_damper(t,state,b,k,m);
options=odeset('RelTol',10^-10,'AbsTol',10^-11);
[t_ode,state]=ode45(f,tspan,[0;0],options);

%% Linear Parameterization
%  In order to linearly parameterize the system, we need
%  to filter the system output, since we cannot measure
%  values of the output's derivatives. Thus, we will 
%  define the polynomial: L(s)=s^2+L1*s+L2 in order to be
%  used for the filtering. The parameters L1,L2 are selected:
L1=2*0.2; L2=0.2^2; 

%  The linearly parameterized system can be written as y=theta*phi, 
%  where:   theta*=[ b/m-L1, k/m-L2, 1/m ] and phi=[-s*y; -y; u]

%  For the implementation of the LSA we need a fixed timestep, 
%  so we define a new timespan: lsa (Least Squares Algorithm)
t_lsa=0:0.1:10; 
%  The input function (u) on the new timespan
u=5*sin(t_lsa)+10.5;
%  The output function (x1=y) on the new timespan
y=interp1(t_ode, state(:,1), t_lsa);
%  On this timespan, we will filter the Output (y) and Input (u) Functions.
[Phi1,~]=lsim(tf([1,0],[1,L1,L2]),-y,t_lsa, 0);
[Phi2,~]=lsim(tf([0,1],[1,L1,L2]),-y,t_lsa,0);
[Phi3,~]=lsim(tf([0,1],[1,L1,L2]), u,t_lsa, 10.5);
phi=[Phi1';Phi2';Phi3'];

%% Least Squares Algorithm
%  Now that we calculated phi and we also have y, we can apply the
%  LSA in order to find the best value for theta.
%  The LSA is implemented in the summation form:
sum1=zeros(3,3);  
sum2=zeros(3,1);
N=length(t_lsa);
for i=1:N
   sum1=sum1+phi(:,i)*phi(:,i).';
   sum2=sum2+phi(:,i)*y(i);
end
sum1=sum1/N;
sum2=sum2/N;
%  Finally, the minimizing Argument:
theta_0=(sum1)\(sum2);
%  And based on theta*, we can find the estimated parameters:
m_hat=1/theta_0(3);
b_hat=(L1+theta_0(1))*m_hat;
k_hat=(L2+theta_0(2))*m_hat;
   fprintf("\n m=%.2f\n The estimation of m = %.6f\n Estimation Error %% = %.6f %%\n" ...
                              ,m,m_hat, abs(m-m_hat)*100/m)
   fprintf("\n b=%.2f\n The estimation of b = %.6f\n Estimation Error %% = %.6f %%\n" ...
                              ,b,b_hat, abs(b-b_hat)*100/b)
   fprintf("\n k=%.2f\n The estimation of k = %.6f\n Estimation Error %% = %.6f %%\n" ...
                              ,k,k_hat, abs(k-k_hat)*100/k)
   theta=[b/m,k/m,1/m]; theta_0=theta_0+[L1;L2;0];
   fprintf("\n theta=[ ");fprintf("%.6f  ",theta); fprintf("]\n")
   fprintf("\n Estimation of theta=[ ");fprintf("%.6f  ",theta_0); fprintf("]\n\n")
