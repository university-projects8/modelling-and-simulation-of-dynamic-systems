%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr

% This is a small sensitivity analysis on the effect of spike order
% on the estimation of Least Squares Algorithm. It creates a table with
% the estimated values of RC (theta(1), theta(3), theta(5)) and LC 
% (theta(2), theta(4)), writes the table to an xls file and also plots
% the values with respect to the filter order.

% The plots and xls-writing are suppressed as comments.

%This file calls a2_f_spikes.m function.

%Table creation
colnames={'spike','RC_1','RC_2','RC_3','LC_1','LC_2'};
rownames={'#1','#2','#3','#4','#5','#6','#7','#8','#9','#10','#11','#12','#13',...
         '#14','#15','#16','#17','#18','#19','#20', '#21'};
T=array2table(zeros(20,6),'RowNames',rownames,'VariableNames',colnames);
%In the above table we store the values of RC, LC, so the inverse of each
%theta element

%The parameters that are used based on the previous question.
t_step=1e-5;
t_max=2;
pole=75;
spikes=-2:18;
counter=1;
for i=1:length(spikes)
            theta=a2_f_spikes(spikes(i));
            T(counter,:)=array2table([spikes(i),1/theta(1),1/theta(3),...
               1/theta(4), 1/theta(2), 1/theta(5)]);
            counter=counter+1;
end

% writetable(T,'SensitivityQ2Spikes.xls')
fprintf("\n Simulation for different spike order of magnitude")
disp(T)
% figure(1)
% plot(-2:18,T.RC_1,'DisplayName','$RC_{noise}$')
% hold on
% plot(-2:18,0.100288*ones(1,21),'DisplayName','$RC_{clear}$')
% xlabel('Noise Order of Magnitude')
% ylabel('$RC \;of\; \dot{V}_c$','Interpreter', 'latex')
% legend('Location','northwest','Interpreter', 'latex')
% 
% figure(2)
% plot(-2:18,T.RC_2,'DisplayName','$RC_{noise}$')
% hold on
% plot(-2:18,0.100288*ones(1,21),'DisplayName','$RC_{clear}$')
% xlabel('Noise Order of Magnitude')
% ylabel('$RC \;of\; \dot{u}_1$','Interpreter', 'latex')
% legend('Location','northwest','Interpreter', 'latex')
% 
% figure(3)
% plot(-2:18,T.RC_3,'DisplayName','$RC_{noise}$')
% hold on
% plot(-2:18,0.100288*ones(1,21),'DisplayName','$RC_{clear}$')
% xlabel('Noise Order of Magnitude')
% ylabel('$RC\; of \;\dot{u}_2$','Interpreter', 'latex')
% legend('Location','northwest','Interpreter', 'latex')
% 
% figure(4)
% plot(-2:18,T.LC_1,'DisplayName','$LC_{noise}$')
% hold on
% plot(-2:18,3.9935e-8*ones(1,21),'DisplayName','$LC_{clear}$')
% xlabel('Noise Order of Magnitude')
% ylabel('$LC \;of \;V_c$','Interpreter', 'latex')
% legend('Location','northwest','Interpreter', 'latex')
% 
% figure(5)
% plot(-2:18,T.LC_2,'DisplayName','$LC_{noise}$')
% hold on
% plot(-2:18,3.9935e-8*ones(1,21),'DisplayName','$LC_{clear}$')
% xlabel('Noise Order of Magnitude')
% ylabel('$LC \;of\; u_2$','Interpreter', 'latex')
% legend('Location','northwest','Interpreter', 'latex')


