%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr

function [m_hat, k_hat, b_hat] = a1_f(L1, L2)

%% Model Setup
%  The Spring-Damper System can be modelled as follows:
%  y'' + (b/m)*y' + (k/m)*y = (1/m)*u
%  Parameters of the System:
k=2;
b=0.2;
m=15;
%  System Modeling
%  We define the state variables: x1=y, x2=y' and 
%  the system can be modelled in matrix form as follows.
sys=ss([0,1;-k/m,-b/m],[0;1/m],[1,0],0,0.1);
% System Simulation (continuous)
x0=[0 0];   % Initial Conditions
tspan=[0 10]; % Simulation Timespan
f=@(t,state)spring_damper(t,state,b,k,m);
options=odeset('RelTol',10^-10,'AbsTol',10^-11);
[t_ode,state]=ode45(f,tspan,[0;0],options);

%% Linear Parameterization
%  In order to linearly parameterize the system, we need
%  to filter the system output, since we cannot measure
%  values of the output's derivatives. Thus, we will 
%  define the polynomial: L(s)=s^2+L1*s+L2 in order to be
%  used for the filtering. The parameters L1,L2 are  given as inputs.

%  The linearly parameterized system can be written as y=theta*phi, 
%  where:   theta*=[ b/m-L1, k/m-L2, 1/m ] and phi=[-s*y; -y; u]

%  For the implementation of the LSA we need a fixed timestep, 
%  so we define a new timespan: lsa (Least Squares Algorithm)
t_lsa=0:0.1:10; 
%  The input function (u) on the new timespan
u=5*sin(t_lsa)+10.5;
%  The output function (x1=y) on the new timespan
y=interp1(t_ode, state(:,1), t_lsa);
%  On this timespan, we will filter the Output (y) and Input (u) Functions.
[Phi1,~]=lsim(tf([1,0],[1,L1,L2]),-y,t_lsa, 0);
[Phi2,~]=lsim(tf([0,1],[1,L1,L2]),-y,t_lsa,0);
[Phi3,~]=lsim(tf([0,1],[1,L1,L2]), u,t_lsa, 10.5);
phi=[Phi1';Phi2';Phi3'];

%% Least Squares Algorithm
%  Now that we calculated phi and we also have y, we can apply the
%  LSA in order to find the best value for theta.
%  The LSA is implemented in the summation form:
sum1=zeros(3,3);  
sum2=zeros(3,1);
N=length(t_lsa);
for i=1:N
   sum1=sum1+phi(:,i)*phi(:,i).';
   sum2=sum2+phi(:,i)*y(i);
end
sum1=sum1/N;
sum2=sum2/N;
%  Finally, the minimizing Argument:
theta_0=(sum1)\(sum2);
%  And based on theta*, we can find the estimated parameters:
m_hat=1/theta_0(3);
b_hat=(L1+theta_0(1))*m_hat;
k_hat=(L2+theta_0(2))*m_hat;

end
