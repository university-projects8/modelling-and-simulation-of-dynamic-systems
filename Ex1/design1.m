%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr

% This is a small sensitivity analysis on the effect of filter design
% on the estimation of Least Squares Algorithm.  For different filters
% based on filter pole-placement as a double negative real pole, we calculate
% the estimations of the LSA and store them in xls files. The outputs are suppressed.

% The plots and xls-writing are suppressed as comments.
colnames={'pole','m','b','k', 'm_percent', 'b_percent','k_percent'};

% This function calls the a1_f.m function.
rownames={'#1','#2','#3','#4','#5','#6'};
T=array2table(zeros(6,7),'RowNames',rownames,'VariableNames',colnames);

poles=[100,10,1,0.1, 0.05, 0.01];
counter=1;  
for i=1:length(poles)
   [m,k,b]=a1_f(2*poles(i),poles(i)^2);
   T(counter,:)=array2table([poles(i),m,b,k, abs(15-m)*100/15,...
      abs(0.2-b)*100/0.2, abs(2-k)*100/k]);
   counter=counter+1;
end
%writetable(T,'PoleSensitivityQ1.xls')
fprintf("\n First Round of Estimations \n")
disp(T)
rownames={'#1','#2','#3','#4','#5','#6','#7','#8','#9','#10'};

T2=array2table(zeros(10,7),'RowNames',rownames,'VariableNames',colnames);
poles=1:-0.1:0.1;
counter=1;  
for i=1:length(poles)
   [m,k,b]=a1_f(2*poles(i),poles(i)^2);
   T2(counter,:)=array2table([poles(i),m,b,k,abs(15-m)*100/15, ...
      abs(0.2-b)*100/0.2, abs(2-k)*100/k]);
   counter=counter+1;
end
%writetable(T2,'PoleSensitivityQ1Round2.xls')
fprintf("\n Second Round of Estimations \n")
disp(T2)
