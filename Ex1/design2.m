%% Modelling and Simulation of Dynamic Systems Assignment 1 Part 1
%  Jason George Velentzas, AEM: 8785, velentzas@ece.auth.gr

% This is a small sensitivity analysis on the parameters t_max, t_step, L1 and L2
% and how they affect the estimation of the Least Squares Algorithm.
% t_step is the step size of the measurements.
% t_max is the maximum moment that a measurement is taken.
% pole: the filter is designed to have a double negative real pole. This is it.

%The outputs are stored in a table and this table is written to an xls file.

%After the 1st Round of Sensitivity Analysis a 2nd one is held on 1 parameter.

%The output writings are suppressed as comments.

%This file calls a2_f.m function.
colnames={'t_step','t_max','double_pole','RC_1','RC_2','RC_3','LC_1','LC_2'};
rownames={'#1','#2','#3','#4','#5','#6','#7','#8','#9','#10','#11',...
   '#12','#13','#14','#15','#16','#17','#18','#19','#20','#21','#22',...
   '#23','#24','#25','#26','#27','#28','#29','#30','#31','#32','#33',...
   '#34','#35','#36','#37','#38','#39','#40','#41','#42','#43','#44','#45'};
T=array2table(zeros(45,8),'RowNames',rownames,'VariableNames',colnames);
t_step=[1e-3,1e-4,1e-5];
t_max=[2,4,10];
pole=[10,100,150,200 1000];
counter=1;
for i=1:length(t_step)
   for j=1:length(t_max)
      for k=1:length(pole)
            theta=a2_f(t_step(i),t_max(j),2*pole(k), pole(k)^2);
            T(counter,:)=array2table([t_step(i),t_max(j),pole(k),1/theta(1),1/theta(3),...
               1/theta(4), 1/theta(2), 1/theta(5)]);
            counter=counter+1;
      end
   end
end

%writetable(T,'SensitivityQ2.xls')

% This output is being suppressed due to being 45 rows long
%fprintf("\n First Round of Simulations\n")
%disp(T)

rownames={'#1','#2','#3','#4','#5','#6','#7','#8','#9','#10'};
T2=array2table(zeros(10,8),'RowNames',rownames,'VariableNames',colnames);
t_step=1e-5;
t_max=2;
pole=[75,100,125,150,175,200,225,250,275,300];
counter=1;
for i=1:length(t_step)
   for j=1:length(t_max)
      for k=1:length(pole)
            theta=a2_f(t_step(i),t_max(j),2*pole(k), pole(k)^2);
            T2(counter,:)=array2table([t_step(i),t_max(j),pole(k),1/theta(1),1/theta(3),...
               1/theta(4), 1/theta(2), 1/theta(5)]);
            counter=counter+1;
      end
   end
end

%writetable(T2,'SensitivityQ2Round2.xls')
fprintf("\n Second Round of Simulations\n")
disp(T2)
